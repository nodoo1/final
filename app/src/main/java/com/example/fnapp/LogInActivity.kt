package com.example.fnapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        auth = Firebase.auth
        login_button.setOnClickListener {
            logIn()
        }
        login_goSingUp_button.setOnClickListener {
            val i = Intent(this,SignUpActivity::class.java )
            startActivity(i)
        }
    }
    private fun logIn(){
        val email = login_email.text.toString()
        val password = login_password.text.toString()
        if(email.isNotEmpty()&&password.isNotEmpty()){
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                            d("logIn", "signInWithEmail:success")
                        val user = auth.currentUser
                        val intent = Intent(this,MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    } else {
                        // If sign in fails, display a message to the user.
                        d("logIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed." + task.exception,
                            Toast.LENGTH_SHORT).show()
                    }

                }
        }else{
            Toast.makeText(this, "Please Fill All Fields", Toast.LENGTH_SHORT).show()
        }
    }
}