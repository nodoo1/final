package com.example.fnapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        generate_bt.setOnClickListener {
            resultNumb()
        }
        clear_bt.setOnClickListener {
            start_num_et.setText("")
            end_numb_et.setText("")
            result_numb_tv.text = ""
        }
    }

    private fun generateNumb(): Int {
        val startNumb = start_num_et.text.toString().toInt()
        val endNumb = end_numb_et.text.toString().toInt()
        return (startNumb..endNumb).random()
    }

    private fun resultNumb() {
        val numb = generateNumb()
        result_numb_tv.text = numb.toString()
    }
}